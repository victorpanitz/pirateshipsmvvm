//
//  ShipDetailsProtocols.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 18/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

protocol ShipDetailsRoutering: AnyObject {
    func dismiss()
}

protocol ShipDetailsView: AnyObject {
    func showPirateGreeting(_ text: String)
}
