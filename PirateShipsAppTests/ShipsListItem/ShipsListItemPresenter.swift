//
//  ShipsListItemPresenter.swift
//  PirateShipsAppTests
//
//  Created by Victor Magalhaes on 17/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import XCTest

@testable import PirateShipsApp

private class ShipsListItemPresenterTests: XCTestCase {

    private var view: ShipsListItemViewSpy!
    private var sut_presenter: ShipsListItemPresenter!
    
    func setup(title: String?, image: String?, price: Double?) {
        view = ShipsListItemViewSpy()
        sut_presenter = ShipsListItemPresenter(title: title, image: image, price: price)
        sut_presenter.attachView(view)
    }

    func test_when_view_attach() {
        setup(title: "title value", image: "image value", price: 110)
        
        XCTAssert(view.setImageCalled == true)
        XCTAssert(view.imagePassed == "image value")
        
        XCTAssert(view.setTitleCalled == true)
        XCTAssert(view.titlePassed == "title value")
        
        XCTAssert(view.setPriceCalled == true)
        XCTAssert(view.pricePassed == "$110.00")
    }
    
    func test_when_view_attach_with_nil_properties() {
        setup(title: nil, image: nil, price: nil)

        XCTAssert(view.setImageCalled == true)
        XCTAssert(view.imagePassed == "https://ichef.bbci.co.uk/images/ic/640x360/p06gsqm7.jpg")
        
        XCTAssert(view.setTitleCalled == true)
        XCTAssert(view.titlePassed == "Unknown")
        
        XCTAssert(view.setPriceCalled == true)
        XCTAssert(view.pricePassed == "$xxx.xx")
    }

    
    func test_when_item_touch_began() {
        setup(title: "", image: "", price: 0)
        sut_presenter.itemTouchBegan()
        
        XCTAssert(view.animateAsResponseCalled == true)
    }
    
    func test_when_item_touch_ended() {
        setup(title: "", image: "", price: 0)
        sut_presenter.itemTouchEnded()
        
        XCTAssert(view.animateToIdentityCalled == true)
    }
    
    func test_when_item_touch_cancelled() {
        setup(title: "", image: "", price: 0)
        sut_presenter.itemTouchCancelled()
        
        XCTAssert(view.animateToIdentityCalled == true)
    }

}

class ShipsListItemViewSpy: ShipsListItemView {
    
    var setImageCalled: Bool?
    var imagePassed: String?
    var setTitleCalled: Bool?
    var titlePassed: String?
    var setPriceCalled: Bool?
    var pricePassed: String?
    var animateAsResponseCalled: Bool?
    var animateToIdentityCalled: Bool?
    
    func setImage(_ text: String) {
        setImageCalled = true
        imagePassed = text
    }
    
    func setTitle(_ text: String) {
        setTitleCalled = true
        titlePassed = text
    }
    
    func setPrice(_ text: String) {
        setPriceCalled = true
        pricePassed = text
    }
    
    func animateAsResponse() {
        animateAsResponseCalled = true
    }
    
    func animateToIdentity() {
        animateToIdentityCalled = true
    }
    
}
