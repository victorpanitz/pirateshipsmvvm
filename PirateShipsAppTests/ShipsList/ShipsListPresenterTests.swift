//
//  PirateShipsAppTests.swift
//  PirateShipsAppTests
//
//  Created by Victor on 12/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import XCTest

@testable import PirateShipsApp

private class ShipsListPresenterTests: XCTestCase {

    private var view: ShipsListViewSpy!
    private var router: ShipsListRouterSpy!
    private var repository: ShipsListRepositoryInputSpy!
    private var sut_presenter: ShipsListPresenter!
    
    override func setUp() {
        view = ShipsListViewSpy()
        router = ShipsListRouterSpy()
        repository = ShipsListRepositoryInputSpy()
        sut_presenter = ShipsListPresenter(router: router, repository: repository)
        sut_presenter.attatchView(view)
    }

    func test_when_view_did_load() {
        XCTAssert(view.showLoadingCalled == true)
        XCTAssert(repository.fetchPirateShipsCalled == true)
    }
    
    func test_when_cell_triggered() {
        sut_presenter.retrieveLocalDataSucceeded(ships: [.dummy_navigation])
        sut_presenter.cellGotTriggered(index: 0)
        
        XCTAssert(router.navigateToShipDetailsCalled == true)
        XCTAssert(router.shipTitlePassed == "ship title value")
        XCTAssert(router.shipDescriptionPassed == "ship description value")
        XCTAssert(router.shipImagePassed == "ship image value")
        XCTAssert(router.shipGreetingTypePassed == .yo)
        XCTAssert(router.shipPricePassed == 14.2)
    }
    
    func test_when_fetch_data_succeeded() {
        sut_presenter.fetchDataSucceeded(ships: Ship.dummy_ships_data)
        
        XCTAssert(view.hideLoadingCalled == true)
        
        XCTAssert(repository.updateShipsListCalled == true)
        XCTAssert(repository.elementsPassed?.count == 3)
        
        XCTAssert(repository.elementsPassed?[0].title == "ship1 title value")
        XCTAssert(repository.elementsPassed?[0].description == "ship1 description value")
        XCTAssert(repository.elementsPassed?[0].imageUrl == "ship1 imageUrl value")
        XCTAssert(repository.elementsPassed?[0].type == .yo)
        XCTAssert(repository.elementsPassed?[0].price == 10.3)
        
        XCTAssert(repository.elementsPassed?[1].title == "ship2 title value")
        XCTAssert(repository.elementsPassed?[1].description == "ship2 description value")
        XCTAssert(repository.elementsPassed?[1].imageUrl == "ship2 imageUrl value")
        XCTAssert(repository.elementsPassed?[1].type == .yo)
        XCTAssert(repository.elementsPassed?[1].price == 8.5)
        
        XCTAssert(repository.elementsPassed?[2].title == "ship3 title value")
        XCTAssert(repository.elementsPassed?[2].description == "ship3 description value")
        XCTAssert(repository.elementsPassed?[2].imageUrl == "ship3 imageUrl value")
        XCTAssert(repository.elementsPassed?[2].type == .yo)
        XCTAssert(repository.elementsPassed?[2].price == 4.4)
    }
    
    func test_when_fetch_data_succeeded_with_empty_list() {
        sut_presenter.fetchDataSucceeded(ships: [])
        
        XCTAssert(view.hideLoadingCalled == true)
        XCTAssert(repository.updateShipsListCalled == true)
        XCTAssert(repository.elementsPassed?.count == 0)
    }
    
    func test_when_fetch_data_failed() {
        sut_presenter.fetchDataFailed(error: "error value")
        
        XCTAssert(view.hideLoadingCalled == true)
        XCTAssert(view.showErrorCalled == true)
        XCTAssert(view.errorMessagePassed == "error value")
    }
    
    func test_when_retrive_local_data_succeeded() {
        sut_presenter.retrieveLocalDataSucceeded(ships: Ship.dummy_ships_data)
        
        XCTAssert(view.updateShipsListCalled == true)
        
        XCTAssert(view.elementsPassed?.count == 3)
        
        XCTAssert(view.elementsPassed?[0].id == 0103)
        XCTAssert(view.elementsPassed?[0].title == "ship3 title value")
        XCTAssert(view.elementsPassed?[0].description == "ship3 description value")
        XCTAssert(view.elementsPassed?[0].imageUrl == "ship3 imageUrl value")
        XCTAssert(view.elementsPassed?[0].type == .yo)
        XCTAssert(view.elementsPassed?[0].price == 4.4)
        
        XCTAssert(view.elementsPassed?[1].id == 0102)
        XCTAssert(view.elementsPassed?[1].title == "ship2 title value")
        XCTAssert(view.elementsPassed?[1].description == "ship2 description value")
        XCTAssert(view.elementsPassed?[1].imageUrl == "ship2 imageUrl value")
        XCTAssert(view.elementsPassed?[1].type == .yo)
        XCTAssert(view.elementsPassed?[1].price == 8.5)
        
        XCTAssert(view.elementsPassed?[2].id == 0101)
        XCTAssert(view.elementsPassed?[2].title == "ship1 title value")
        XCTAssert(view.elementsPassed?[2].description == "ship1 description value")
        XCTAssert(view.elementsPassed?[2].imageUrl == "ship1 imageUrl value")
        XCTAssert(view.elementsPassed?[2].type == .yo)
        XCTAssert(view.elementsPassed?[2].price == 10.3)
    }
    
    func test_when_retrive_local_data_failed() {
        sut_presenter.retrieveLocalDataFailed(error: "error value")
        
        XCTAssert(view.updateShipsListCalled == nil)
        XCTAssert(view.elementsPassed?.count == nil)
        XCTAssert(view.showErrorCalled == true)
        XCTAssert(view.errorMessagePassed == "error value")
    }
    
    func test_when_update_ships_list_succeeded(){
        sut_presenter.updateShipsListSucceeded()
        
        XCTAssert(repository.retrieveShipsCalled == true)
    }
    
    func test_when_update_ships_list_failed(){
        sut_presenter.updateShipsListFailed(error: "error value")
        
        XCTAssert(view.errorMessagePassed == "error value")
        XCTAssert(view.showErrorCalled == true)
    }
    
    func test_when_refresh_control_triggered() {
        sut_presenter.refreshControlTriggered()
        
        XCTAssert(view.showLoadingCalled == true)
        XCTAssert(repository.fetchPirateShipsCalled == true)
    }

}

fileprivate class ShipsListViewSpy: ShipsListView {
    
    var titlePassed: String?
    var updateShipsListCalled: Bool?
    var elementsPassed: [Ship]?
    var hideLoadingCalled: Bool?
    var showLoadingCalled: Bool?
    var showErrorCalled: Bool?
    var errorMessagePassed: String?
    
    func updateShipsList(_ ships: [Ship]) {
        updateShipsListCalled = true
        elementsPassed = ships
    }
    
    func showLoading() {
        showLoadingCalled = true
    }
    
    func hideLoading() {
        hideLoadingCalled = true
    }
    
    func showError(message: String) {
        showErrorCalled = true
        errorMessagePassed = message
    }
}

fileprivate class ShipsListRouterSpy: ShipsListRoutering {
    var navigateToShipDetailsCalled: Bool?
    var shipTitlePassed: String?
    var shipDescriptionPassed: String?
    var shipImagePassed: String?
    var shipGreetingTypePassed: PirateGreeting?
    var shipPricePassed: Double?
    
    func navigateToShipDetails(ship: Ship) {
        navigateToShipDetailsCalled = true
        shipTitlePassed = ship.title
        shipDescriptionPassed = ship.description
        shipImagePassed = ship.imageUrl
        shipGreetingTypePassed = ship.type
        shipPricePassed = ship.price
    }
}

fileprivate class ShipsListRepositoryInputSpy: ShipsListRepositoryInput {
    
    var output: ShipsListRepositoryOutput?
    var updateShipsListCalled: Bool?
    var elementsPassed: [Ship]?
    var fetchPirateShipsCalled: Bool?
    var retrieveShipsCalled: Bool?
    
    func fetchPirateShips() {
        fetchPirateShipsCalled = true
    }
    
    func updateShipsList(_ ships: [Ship]) {
        updateShipsListCalled = true
        elementsPassed = ships
    }
    
    func retrieveShips() {
        retrieveShipsCalled = true
    }

}

extension Ship {
    static var dummy_navigation: Ship {
        return Ship (
            id: 0101,
            title: "ship title value",
            imageUrl: "ship image value",
            description: "ship description value",
            type: .yo,
            price: 14.2
        )
    }
    
    static var dummy_ships_data: [Ship] {
        return [
            Ship (
                id: 0101,
                title: "ship1 title value",
                imageUrl: "ship1 imageUrl value",
                description: "ship1 description value",
                type: .yo,
                price: 10.3
            ),
            Ship (
                id: 0102,
                title: "ship2 title value",
                imageUrl: "ship2 imageUrl value",
                description: "ship2 description value",
                type: .yo,
                price: 8.5
            ),
            Ship (
                id: 0103, 
                title: "ship3 title value",
                imageUrl: "ship3 imageUrl value",
                description: "ship3 description value",
                type: .yo,
                price: 4.4
            )
            
        ]
    }
}
