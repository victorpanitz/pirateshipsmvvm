//
//  ShipDetailsRouter.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 18/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import UIKit

final class ShipDetailsRouter: ShipDetailsRoutering {
    
    private weak var view: UIViewController?
    private let ship: Ship
    
    init(ship: Ship) {
        self.ship = ship
    }
    
    func makeViewController() -> UIViewController {
        
        let viewModel = ShipDetailsViewModel(router: self, ship: ship)
        let viewController = ShipDetailsViewController(viewModel: viewModel)
        self.view = viewController
        
        return viewController
    }
    
    func dismiss() {
        view?.dismiss(animated: true, completion: nil)
    }
}
