//
//  ShipDetailsPresenter.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 18/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class ShipDetailsViewModel {
    
    private let ship: Ship
    private let bag = DisposeBag()
    
    private let router: ShipDetailsRoutering
    
    private(set) lazy var title = BehaviorRelay<String>(value: "")
    private(set) lazy var description = BehaviorRelay<String>(value: "")
    private(set) lazy var image = BehaviorRelay<String>(value: "")
    private(set) lazy var price = BehaviorRelay<String>(value: "")
    private(set) lazy var showAlert = PublishSubject<String>()
    
    let greetingTapped = PublishSubject<Void>()
    let closeButtonTapped = PublishSubject<Void>()
    
    init(router: ShipDetailsRoutering, ship: Ship) {
        self.ship = ship
        self.router = router
        
        setValues()
        setSubscriptions()
    }
    
    private func setValues() {
        title.accept(ship.title ?? "Unknown")
        
        description.accept(ship.description ?? "Unknown")
        
        image.accept(ship.imageUrl ?? "https://ichef.bbci.co.uk/images/ic/640x360/p06gsqm7.jpg")
        
        price.accept({
            guard  let price = ship.price, price > 0 else { return "$xxx.xx" }
            return  String(format: "$%.2f", price)
            }()
        )
    }
    
    func setSubscriptions() {
        greetingTapped
            .subscribe({ [weak self] (_) in
                self.map { $0.showAlert.onNext( $0.ship.type?.rawValue ?? "" ) }
            })
            .disposed(by: bag)
        
        closeButtonTapped
            .subscribe { [weak self] (_) in
                self.map { $0.router.dismiss() }
            }
            .disposed(by: bag)
    }
}
