//
//  ViewController.swift
//  PirateShipsApp
//
//  Created by Victor on 12/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import UIKit

final class ShipsListViewController: UIViewController {

    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let collection = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        collection.register(ShipsListItemCell.self, forCellWithReuseIdentifier: "shipCell")
        collection.register(ShipsListHeaderCell.self, forCellWithReuseIdentifier: "headerCell")
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = UIColor.clear
        collection.contentMode = .scaleAspectFill
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.clipsToBounds = true
        collection.isUserInteractionEnabled = true
        collection.contentInset = UIEdgeInsets(top: 30, left: 20, bottom: 30, right: 20)
        
        return collection
    }()
    
    private let refreshControl = UIRefreshControl()
    
    private var ships = [Ship]()
    
    private let presenter: ShipsListPresenter
    
    init(presenter: ShipsListPresenter) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) { return nil }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
    
        setupCollectionView()
        setupRefreshControl()
        
        presenter.attatchView(self)
    }
    
    // Mark: Private methods
    
    private func setupCollectionView() {
        view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
    }
    
    private func setupRefreshControl() {
        refreshControl.tintColor = .cyan
        collectionView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshTriggered), for: .valueChanged)
    }
    
    @objc private func refreshTriggered() {
        presenter.refreshControlTriggered()
    }

}

// Mark: ShipsListView

extension ShipsListViewController: ShipsListView {

    func updateShipsList(_ ships: [Ship]) {
        self.ships = ships
        
        DispatchQueue.main.async { [weak self] in
            self?.refreshControl.endRefreshing()
            self?.collectionView.reloadData()
        }
    }
    
    func showError(message: String) {
        let alert = UIAlertController(title: "Ops!", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoading() {
        LoadingManager.show(in: self)
    }
    
    func hideLoading() {
        LoadingManager.hide()
    }

}

// Mark: CollectionView

extension ShipsListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ships.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "headerCell", for: indexPath) as? ShipsListHeaderCell else { return UICollectionViewCell() }
            
            return cell
            
        default:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shipCell", for: indexPath) as? ShipsListItemCell else { return UICollectionViewCell() }
         
            
            let ship = ships[indexPath.row - 1]
            let presenter = ShipsListItemPresenter(title: ship.title, image: ship.imageUrl, price: ship.price)
            cell.attachPresenter(presenter)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.row {
        case 0:
            return CGSize(width: self.view.frame.width * 0.8 , height: self.view.frame.height * 0.2)
        default:
            return CGSize(width: self.view.frame.width * 0.8 , height: self.view.frame.height * 0.5)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 40
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.cellGotTriggered(index: indexPath.row - 1)
    }
}
