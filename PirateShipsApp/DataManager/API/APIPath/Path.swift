//
//  Path.swift
//  PirateShipsApp
//
//  Created by Victor Magalhaes on 13/03/19.
//  Copyright © 2019 Victor. All rights reserved.
//

import Foundation

enum PathURL: String {

    case base = "https://assets.shpock.com/mobile/interview-test/"
    
    case pirateShips = "pirateships"
    
}
